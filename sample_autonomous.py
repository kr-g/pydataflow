import datetime

from dataflow import CellDataFlow
from dataflow.state import State

cf = CellDataFlow()

c1 = cf.create_cell("c1")


# the cell autonomous function is called with the bound cell as parameter


def timed_func(cell):
    # in this example cell.state contains a State object
    # State supports dict and namespace notation
    # but it is possible to store any type in cell.state
    starttime = cell.state.starttime

    if starttime:

        # like with a dict this sets a default value
        # in the State object if not present
        s_delay = cell.state.setdefault("delay", 3)
        # the will return the value too
        delay = cell.state.delay
        # same value
        assert s_delay == delay

        now = datetime.datetime.now()
        dif = now - starttime
        print("delay total", delay, "delayed already", dif)

        if dif.seconds >= delay:
            cell.val = "signaled"
            # State supports dict and namespace notation
            # set the starttime
            cell.state["starttime"] = None
            # same value
            assert cell.state.starttime == None


# delay for 3 sec (default value in function above )
c1.set_autfunc(
    timed_func,
    State(
        {
            "starttime": datetime.datetime.now(),
        }
    ),
)


def cell_wait_func(c, v):
    print("c2_wait", c, v)
    # this cell func does nothix just print cell val
    # return a value to trigger other cells in the flow
    # here the receiver is c3 (see below)
    return v


c2 = cf(id="c2", watching=c1, func=cell_wait_func)


_active = True


def stop_flow():
    global _active
    _active = False
    # this will be handed over to c3.val
    return "signaled-received"


c3 = cf(id="c3", watching=[c2], func=lambda c, v: stop_flow())

# loop until 3 seconds are over
while _active:

    cf.loop()

# print all cells in the flow
# cell state is not in repr() because its real type is unknown
# so print out separately in this sample
for cell in cf.cells:
    print("cell", cell, cell.state)
